<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSslExpiration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ssl_expiration', function (Blueprint $table) {
            $table->id();
            $table->string('url')->nullable();
            $table->string('domain');
            $table->string('vendor')->nullable();
            $table->date('date_start')->nullable();
            $table->date('date_expire')->nullable();
            $table->longText('remark')->nullable();
            $table->longText('ca_csr')->nullable();
            $table->longText('ca_key')->nullable();
            $table->longText('ca_crt')->nullable();
            $table->longText('ca_int')->nullable();
            $table->string('csr_country_code')->nullable();
            $table->string('csr_province')->nullable();
            $table->string('csr_locality')->nullable();
            $table->string('csr_organization')->nullable();
            $table->string('csr_organization_unit')->nullable();
            $table->string('csr_hostname')->nullable();
            $table->string('csr_email')->nullable();
            $table->string('csr_password')->nullable();
            $table->string('csr_opt_company')->nullable();
            $table->boolean('sent_notification')->default(false);
            $table->boolean('sent_warning')->default(false);
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ssl_expiration');
    }
}
