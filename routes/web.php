<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix'=>'api/v1'], function () use ($router) {
    // SSL Expiration routes
    $router->group(['prefix'=>'ssl'], function () use ($router) {
        $router->get('/', 'API\SSLExpirationController@index');
        $router->get('{id}', 'API\SSLExpirationController@show');
        $router->post('/', 'API\SSLExpirationController@store');
        $router->put('{id}', 'API\SSLExpirationController@update');
        $router->put('{id}/send_notification', 'API\SSLExpirationController@send_notification');
        $router->put('{id}/send_warning', 'API\SSLExpirationController@send_warning');
        $router->get('{id}/ca_show', 'API\SSLExpirationController@ca_show');
        $router->put('{id}/ca_update', 'API\SSLExpirationController@ca_update');
        $router->get('{id}/csr_show', 'API\SSLExpirationController@csr_show');
        $router->put('{id}/csr_update', 'API\SSLExpirationController@csr_update');
        $router->delete('{id}', 'API\SSLExpirationController@destroy');
    });
});