<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SSLExpiration extends Model
{
    protected $table = 'ssl_expiration';
}
