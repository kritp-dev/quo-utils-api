<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SSL extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'url' => $this->url,
            'domain' => $this->domain,
            'vendor' => $this->vendor,
            'date_start' => $this->date_start,
            'date_expire' => $this->date_expire,
            'remark' => $this->remark,
            'sent_notification' => $this->sent_notification,
            'sent_warning' => $this->sent_warning,
            'active' => $this->active,
        ];
        return $data;
    }
}
