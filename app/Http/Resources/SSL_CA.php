<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SSL_CA extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'ca_csr' => $this->ca_csr,
            'ca_key' => $this->ca_key,
            'ca_crt' => $this->ca_crt,
            'ca_int' => $this->ca_int,
        ];
        return $data;
    }
}
