<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SSL_CSR extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'csr_country_code' => $this->csr_country_code,
            'csr_province' => $this->csr_province,
            'csr_locality' => $this->csr_locality,
            'csr_organization' => $this->csr_organization,
            'csr_organization_unit' => $this->csr_organization_unit,
            'csr_hostname' => $this->csr_hostname,
            'csr_email' => $this->csr_email,
            'csr_password' => $this->csr_password,
            'csr_opt_company' => $this->csr_opt_company,
        ];
        return $data;
    }
}
