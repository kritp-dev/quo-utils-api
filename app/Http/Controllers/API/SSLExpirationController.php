<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\SSLExpiration;
use App\Http\Resources\SSL;
use App\Http\Resources\SSL_CA;
use App\Http\Resources\SSL_CSR;

class SSLExpirationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $msg = 'Record not found.';
        $ssl = SSL::collection(SSLExpiration::all());
        if ($ssl->count()>0)
            $msg = $ssl->count().' record'.(($ssl->count()>1) ? 's' : '').' found.';

        return response()->json(['message' => $msg, 'data' => $ssl]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $msg = 'Record fail to save';
        $ssl = new SSLExpiration;
        $ssl->url = $request->url;
        $ssl->domain = $request->domain;
        $ssl->vendor = htmlentities($request->vendor, ENT_QUOTES);
        $ssl->date_start = $request->date_start;
        $ssl->date_expire = $request->date_expire;
        $ssl->remark = htmlentities($request->remark, ENT_QUOTES);
        $ssl->active = $request->active;
        $ssl->save();
        $msg = 'Succeed';

        return response()->json(['message' => $msg, 'data' => $ssl]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $msg = 'Record not found';
        $ssl = new SSL(SSLExpiration::find($id));
        if ($ssl)
            $msg = 'Succeed';

        return response()->json(['message' => $msg, 'data' => $ssl]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $msg = 'Update failed';
        $ssl = SSLExpiration::find($id);
        if ($ssl) {
            $ssl->url = $request->input('url');
            $ssl->domain = $request->input('domain');
            $ssl->vendor = $request->input('vendor');
            $ssl->date_start = $request->input('date_start');
            $ssl->date_expire = $request->input('date_expire');
            $ssl->remark = $request->input('remark');
            $ssl->active = $request->input('active');
            $ssl->save();
            $msg = 'Succeed';
        }

        return response()->json(['message' => $msg, 'data' => $ssl]);
    }

    /**
     * Update the notification sending status to resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function send_notification(Request $request, $id)
    {
        $msg = 'Send notification fail.';
        $ssl = SSLExpiration::find($id);
        if ($ssl) {
            $ssl->sent_notification = $request->input('sent_notification');
            $ssl->save();

            $ssl = new SSL($ssl);
            $msg = 'Sent notification successfully.';
        }

        return response()->json(['message' => $msg, 'data' => $ssl]);
    }

    /**
     * Update the warning sending status to resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function send_warning(Request $request, $id)
    {
        $msg = 'Send warning fail.';
        $ssl = SSLExpiration::find($id);
        if ($ssl) {
            $ssl->sent_warning = $request->input('sent_warning');
            $ssl->save();

            $ssl = new SSL($ssl);
            $msg = 'Sent warning successfully.';
        }

        return response()->json(['message' => $msg, 'data' => $ssl]);
    }

    /**
     * Display the specified Certificate (CA) information.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ca_show($id)
    {
        $msg = 'Record not found';
        $ssl = new SSL_CA(SSLExpiration::find($id));
        if ($ssl)
            $msg = 'Succeed';

        return response()->json(['message' => $msg, 'data' => $ssl]);
    }

    /**
     * Update the Certificate (CA) information to resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ca_update(Request $request, $id)
    {
        $msg = 'Record not found';
        $ssl = SSLExpiration::find($id);
        if ($ssl) {
            $ssl->ca_csr = $request->input('ca_csr');
            $ssl->ca_key = $request->input('ca_key');
            $ssl->ca_crt = $request->input('ca_crt');
            $ssl->ca_int = $request->input('ca_int');
            $ssl->save();

            $ssl = new SSL_CA($ssl);
            $msg = 'Record update successfully.';
        }

        return response()->json(['message' => $msg, 'data' => $ssl]);
    }

    /**
     * Display the specified Certificate Signing Request (CSR) information.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function csr_show($id)
    {
        $msg = 'Record not found';
        $ssl = new SSL_CSR(SSLExpiration::find($id));
        if ($ssl)
            $msg = 'Succeed';

        return response()->json(['message' => $msg, 'data' => $ssl]);
    }

    /**
     * Update the Certificate Signing Request (CSR) information to resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function csr_update(Request $request, $id)
    {
        $msg = 'Record not found.';
        $ssl = SSLExpiration::find($id);
        if ($ssl) {
            $ssl->csr_country_code = $request->input('csr_country_code');
            $ssl->csr_province = $request->input('csr_province');
            $ssl->csr_locality = $request->input('csr_locality');
            $ssl->csr_organization = $request->input('csr_organization');
            $ssl->csr_organization_unit = $request->input('csr_organization_unit');
            $ssl->csr_hostname = $request->input('csr_hostname');
            $ssl->csr_email = $request->input('csr_email');
            $ssl->csr_password = $request->input('csr_password');
            $ssl->csr_opt_company = $request->input('csr_opt_company');
            $ssl->save();

            $ssl = new SSL_CSR($ssl);
            $msg = 'Record update successfully.';
        }

        return response()->json(['message' => $msg, 'data' => $ssl]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $msg = 'Record not found.';
        $ssl = SSLExpiration::find($id);
        if ($ssl) {
            $ssl->delete();
            $msg = 'Record remove successfully.';
        }

        return response()->json(['message' => $msg]);
    }
}
